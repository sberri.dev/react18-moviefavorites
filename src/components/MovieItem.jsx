import PropTypes from "prop-types";
import "./MovieItem.css";
import { useEffect, useState } from "react";

// Définition du composant MovieItem avec "les props" passées en argument
function MovieItem(props) {
  // Destructuration des props pour obtenir les valeurs de title, released, director, et poster
  const { title, released, director, poster } = props;

  // Création d'un état "favorite" avec une valeur vide par défaut
  const [favorite, setFavorite] = useState(() => {
    // Récupèration de la valeur stockée dans le localStorage sous la clé du titre du film
    const storedFavorite = localStorage.getItem(title);
    // Nous pose une condition avec l'opérateur ternaire : Si une valeur est trouvée, la convertit de chaîne de texte à un objet JavaScript, sinon, utilise false
    return storedFavorite ? JSON.parse(storedFavorite) : false;
  });

  // Utilisation de useEffect pour sauvegarder la valeur de "favorite" dans le localStorage à chaque changement
  useEffect(() => {
    localStorage.setItem(title, JSON.stringify(favorite));
  }, [title, favorite]);

  // Fonction appelée lors du clic sur le bouton favori, change la valeur de "favorite"
  function toggleFavorite() {
    setFavorite(!favorite);
  }

  // Retourne l'élément JSX représentant le composant MovieItem
  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>

      {/* Création d'un bouton cliquable pour activer la fonction toggleFavorite */}
      <button type="button" onClick={toggleFavorite}>
        {favorite ? "Remove from favorite" : "Add to favorite"}
      </button>
    </div>
  );
}

// Définition des types attendus avec l'élément "propTypes" pour chaque propriété du composant MovieItem
MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
